# Sumi Configuration

This project contains all of the configuration elements for the Sumi project, including internationalization and localization files, and the web manifest templates. 

It's imported as a git submodule to the [Sumi Home](https://gitlab.com/omnifi/sumi/home) project.